﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.Installer;
using System;
using System.Collections.Generic;
using Castle.Core;
using Castle.DynamicProxy;
using NUnit.Framework;
using Rhino.Mocks;
using Polly;
using Polly.Retry;

namespace DI_Test
{
    public class Program
    {
        private static void Main()
        {

            _container = new WindsorContainer().Install(FromAssembly.This());

            var runner = _container.Resolve<Runner>();

            runner.Run();

        }

        private static IWindsorContainer _container;
    }

    public class Runner
    {
        public void Run()
        {
            _dataDependency.GetData();
        }

        private readonly IDataDependency _dataDependency;

        public Runner(IDataDependency dataDependency)
        {
            _dataDependency = dataDependency;
        }
    }


    public interface IDataDependency
    {
        int GetData();
    }

    public class DatabaseService : IDataDependency
    {
        private int _count = 0;
        public int GetData()
        {
            Console.WriteLine("Hit me!");
            if (_count < 4)
            {
                _count++;
                throw new NotImplementedException();
            }
            return 0;
        }
    }

    public class WaitAndRetryInterceptor<T> : Castle.DynamicProxy.IInterceptor where T : Exception
    {
        private readonly RetryPolicy _policy;
        public void Intercept(IInvocation invocation)
        {
            var nameOfinvocation = typeof(IInvocation).Name;
            var fullNameOfInvocation = typeof(IInvocation).FullName;
            var dictionary = new Dictionary<string, object> { { "methodName", invocation.Method.Name } };

            _policy.Execute(invocation.Proceed, dictionary);
        }

        public WaitAndRetryInterceptor()
        {
            _policy =
                Policy
                    .Handle<T>()
                    .WaitAndRetry(new[]
                    {
                        TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5)
                    });
        }
    }

    public class ControllersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container
                .Register(Component.For<Runner>())
                

                .Register(Component.For<IDataDependency>()
                    .ImplementedBy<DatabaseService>()
                    .LifestyleSingleton()
                    .Interceptors(InterceptorReference.ForKey("wait")).Anywhere
                )

                .Register(Component.For<WaitAndRetryInterceptor<NotImplementedException>>().LifeStyle.Singleton
                .Named("wait")
                )
                ;
        }
    }

    


    [TestFixture]
    public class TestMethods
    {
        [Test]
        public void TestGetData()
        {
            var mockedDataDependency = MockRepository.GenerateMock<IDataDependency>();
            mockedDataDependency.Stub(m => m.GetData()).Return(1000000);
        }

        [Test]
        public void CanCreateValidContainer()
        {
            var container = new WindsorContainer().Install(FromAssembly.This());

            var runner = container.Resolve<Runner>();
        }

        [Test]
        public void TestThreeTimes()
        {
            var container = new WindsorContainer().Install(FromAssembly.This());

            var runner = container.Resolve<Runner>();

            runner.Run();
        }

        [Test]
        public void TestThreeTimesDataLayer()
        {
            var container = new WindsorContainer().Install(FromAssembly.This());

            var runner = container.Resolve<IDataDependency>();

            var result = runner.GetData();

            Assert.AreEqual(0, result);
        }
        [Test]
        public void TestFourTimesDataLayerFailing()
        {
            var container = new WindsorContainer().Install(FromAssembly.This());

            var dataDependency = container.Resolve<IDataDependency>();

            Assert.Throws<NotImplementedException>(() => dataDependency.GetData());
        }
    }
}
